# SafeBuffers
SafeBuffers provides safer wrappers for ComputeBuffer and ComputeShader. 
and making ComputeShader dispatches while leaving
less room for user error.

## Advantages
* Typesafe use of ComputeBuffer and ComputeShader
	* ComputeBuffer now tracks its type and can only be filled with Arrays/Lists/NativeArrays of the same type
	`_indexBuffer = new SafetyBuffer<int>("CullIndexBuffer", bufferSize);`
	`_indexBuffer.SetData(indexes);`
	* ComputeShader Kernels are now structs with information and checks on them rather than an index
	`_clearArgsKernel = _compute.FindKernel("CSClearArgs");`
	`Debug.Log(_clearArgsKernel.ThreadGroupSize);`
* Enforces safe dispatches
	* Run a dispatch with only the Kernel and the buffer to operate on, rather than figuring out the ThreadGroup count manually
		`_clearArgsKernel.DispatchOver(_argsBuffer);`
	* Get compatible sizes for safe dispatch, to avoid out of bounds issues
		`_clearArgsKernel.GetCompatibleBufferSize(Objects.Length);`
* Developed to catch console specific issues
	* ComputeBuffer behavior is different on all platforms. PC is the most stable, making issues hard to spot
	* Designed to catch the type of issues found developing Samurai Punk's 'Feather', 'Table of Tales' and other projects for platforms including PC, OSX, Switch, PS4 and Xbox One
	* All platforms handle out of bounds reads and writes differently. SafeBuffers enforce good behaviour.
	* Some platforms need IndirectArguments to be stored with a stride of sizeof(uint). SafetyBuffer handles this in a way that's hidden from the user.


# Examples
SafeBuffers includes a few examples

## TestGpuCulling
This is a fully featured example of a safe DrawMeshInstancedIndirect process with multiple types of drawn objects and GPU based culling.
Note that as this is an example of the SafeBuffers system the ComputeShader itself is very cut down and doesn't show how to calculate frustums, occlusion or similar.

## TestSafetyBuffers and TestSafetyComputeShader
These are modular examples that come with a corresponding TestDangerBuffers and TestDangerComputeShader example, so that you can compare SafetyBuffer with ComputeBuffer and similar.
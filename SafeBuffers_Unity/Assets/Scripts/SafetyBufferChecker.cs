﻿using System;
using System.Collections.Generic;

public static class SafetyBufferChecker
{
    public interface ISafetyBufferCheck
    {
        void SafetyCheck();
    }

    public static DebugMode GlobalDebugMode = DebugMode.ForceUninitializedPadding | DebugMode.CheckPadding;

    [Flags]
    public enum DebugMode
    {
        None = 0,
        ForceUninitializedBuffer = 1,
        ForceUninitializedPadding = 2,
        CheckPadding = 4,
    }

    private static readonly List<ISafetyBufferCheck> CheckPaddingOn = new List<ISafetyBufferCheck>();
#if MEC_ENABLED
    private static MEC.CoroutineHandle _safetyCheckCallPeriodically;
#endif

#if UNITY_EDITOR
    [UnityEditor.MenuItem("Feather/SafetyCheckBuffers")]
#endif
    public static void SafetyCheckAll()
    {
        foreach (var safetyBuffer in CheckPaddingOn)
        {
            safetyBuffer.SafetyCheck();
        }
    }

    public static void AddToPaddingCheck(ISafetyBufferCheck safetyBuffer)
    {
        CheckPaddingOn.Add(safetyBuffer);
#if MEC_ENABLED
        if (CheckPaddingOn.Count == 1 && !_safetyCheckCallPeriodically.IsValid)
        {
            _safetyCheckCallPeriodically = MEC.Timing.CallPeriodically(float.MaxValue, 1, SafetyCheckAll);
        }
#endif
    }

    public static void RemoveFromPaddingCheck(ISafetyBufferCheck safetyBuffer)
    {
        CheckPaddingOn.Remove(safetyBuffer);
#if MEC_ENABLED
        if (CheckPaddingOn.Count == 0 && _safetyCheckCallPeriodically.IsValid)
        {
            MEC.Timing.KillCoroutines(_safetyCheckCallPeriodically);
            _safetyCheckCallPeriodically = MEC.CoroutineHandle.Null;
        }
#endif
    }
}
﻿using System;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Rendering;

/// <summary>
/// A wrapped Compute shader that handles Kernels nicer and interacts well with SafetyBuffers.
/// </summary>
public readonly struct SafetyComputeShader
{
    internal readonly ComputeShader RawCompute;

    /// <summary>
    /// Create a SafetyComputeShader from a normal ComputeShader, which wraps it in a few useful ways.
    /// </summary>
    /// <param name="computeShader">The source computeShader</param>
    public SafetyComputeShader(ComputeShader computeShader)
    {
        RawCompute = computeShader;
    }

    /// <summary>
    /// Get a Kernel object, which is used to interact with the kernel directly rather than as an index
    /// </summary>
    /// <param name="name">The name of the Kernel (as in '#pragma kernel NAME' in the compute shader)</param>
    /// <returns>An object that represents this kernel on this compute shader</returns>
    public Kernel FindKernel(string name)
    {
        var newKernel = new Kernel(this, name);
        return newKernel;
    }

    /// <summary>
    /// Get a Kernel object, which is used to interact with the kernel directly rather than as an index
    /// </summary>
    /// <param name="name">The name of the Kernel (as in '#pragma kernel NAME' in the compute shader)</param>
    /// <returns>An object that represents this kernel on this compute shader</returns>
    public Kernel this[string name] => FindKernel(name);

    #region PropertySetMethods
    public void SetFloat(string name, float value)
    {
        RawCompute.SetFloat(name, value);
    }
    public void SetInt(string name, int value)
    {
        RawCompute.SetInt(name, value);
    }

    public void SetVectorArray(string name, Vector4[] value)
    {
        RawCompute.SetVectorArray(name, value);
    }

    public void SetFloats(string name, float[] value)
    {
        RawCompute.SetFloats(name, value);
    }

    public void SetVector(string name, Vector4 value)
    {
        RawCompute.SetVector(name, value);
    }
    #endregion

    /// <summary>
    /// A wrapper to access Kernel specific properties without passing int based indexes around.
    /// </summary>
    public readonly struct Kernel
    {
        /// <summary>
        /// The thread group size, as defined in by [numthreads(x,y,z)] in the compute shader
        /// </summary>
        public uint3 ThreadGroupSize { get; }
        /// <summary>
        /// The Kernel's name (as in '#pragma kernel NAME' in the compute shader)
        /// </summary>
        public string Name { get; }

        private readonly SafetyComputeShader SafetyCompute;
        private readonly int _kernelIndex;

        public Kernel(SafetyComputeShader compute, string name)
        {
            Name = name;
            SafetyCompute = compute;
            _kernelIndex = compute.RawCompute.FindKernel(name);
            uint3 size;
            compute.RawCompute.GetKernelThreadGroupSizes(_kernelIndex, out size.x, out size.y, out size.z);
            ThreadGroupSize = size;
        }

        /// <summary>
        /// Dispatch this Kernel of this compute shader, either immediately or via a CommandBuffer.
        /// Make sure you've bound buffers/textures to all referenced slots (even if they won't be used due to flow control).
        /// </summary>
        /// <param name="threadGroupsX">Number of work groups in the X dimension.</param>
        /// <param name="threadGroupsY">Number of work groups in the Y dimension.</param>
        /// <param name="threadGroupsZ">Number of work groups in the Z dimension.</param>
        /// <param name="command">(Optional) If set, writes into the command buffer. If null, dispatches immediately. </param>
        public void Dispatch(int threadGroupsX,
            int threadGroupsY,
            int threadGroupsZ, CommandBuffer command = null)
        {
            if (command == null)
            {
                SafetyCompute.RawCompute.Dispatch(_kernelIndex, threadGroupsX, threadGroupsY, threadGroupsZ);
            }
            else
            {
                command.DispatchCompute(SafetyCompute.RawCompute, _kernelIndex, threadGroupsX, threadGroupsY, threadGroupsZ);
            }
        }

        /// <summary>
        /// A safe way of dispatching to run once per element in a buffer, either immediately or via a CommandBuffer.
        /// Note that this only uses the buffer for length, you must set it with <see cref="SetBuffer{T}(string,SafetyBuffer{T})"/> or similar separately
        /// Make sure you've bound buffers/textures to all referenced slots (even if they won't be used due to flow control).
        /// </summary>
        /// <param name="buffer">The buffer to dispatch over</param>
        /// <param name="command">(Optional) If set, writes into the command buffer. If null, dispatches immediately. </param>
        public void DispatchOver<T>(SafetyBuffer<T> buffer, CommandBuffer command = null) where T : struct
        {
            if (ThreadGroupSize.y != 1 || ThreadGroupSize.z != 1)
            {
                throw new Exception($"SafetyCompute doesn't support multidimensional dispatch " +
                                    $"(on {SafetyCompute.RawCompute.name}.{Name}, size {ThreadGroupSize})");
            }

            if (buffer.Count % ThreadGroupSize.x != 0)
            {
                throw new Exception($"Dispatching {SafetyCompute.RawCompute.name}.{Name} over {buffer.Name} " +
                                    $"but buffer size {buffer.Count} isn't a multiple of {ThreadGroupSize.x}");
            }

            var threadGroupsX = (int)(buffer.Count / ThreadGroupSize.x);
            Dispatch(threadGroupsX, 1, 1, command);
        }

        /// <summary>
        /// Get a size for this buffer that will be safe to call from DispatchOver.
        /// Gets a number that's a multiple of the thread group size and equal to or greater than minimumCount.
        /// </summary>
        /// <param name="minimumCount">The minimum number of elements to include</param>
        /// <returns>A number that's a multiple of the thread group size and equal to or greater than minimumCount.</returns>
        public int GetCompatibleBufferSize(int minimumCount)
        {
            if (ThreadGroupSize.y != 1 || ThreadGroupSize.z != 1)
            {
                throw new Exception($"SafetyCompute doesn't support multidimensional buffer auto size " +
                                    $"(on {SafetyCompute.RawCompute.name}.{Name}, size {ThreadGroupSize})");
            }

            //round this up to be a multiple of the dispatch size
            return (int)(ThreadGroupSize.x * ((minimumCount - 1) / ThreadGroupSize.x + 1));
        }

        #region PropertySetMethods

        /// <summary>
        /// Sets an input or output compute buffer.
        /// </summary>
        /// <param name="nameId">Property name ID, use Shader.PropertyToID to get it.</param>
        /// <param name="buffer">Buffer to set.</param>
        public void SetBuffer<T>(int nameId, SafetyBuffer<T> buffer) where T : struct
        {
            SafetyCompute.RawCompute.SetBuffer(_kernelIndex, nameId, buffer.InternalComputeBuffer);
        }

        /// <summary>
        /// Sets an input or output compute buffer.
        /// </summary>
        /// <param name="name">Name of the buffer variable in shader code.</param>
        /// <param name="buffer">Buffer to set.</param>
        public void SetBuffer<T>(string name, SafetyBuffer<T> buffer) where T : struct
        {
            SafetyCompute.RawCompute.SetBuffer(_kernelIndex, name, buffer.InternalComputeBuffer);
        }

        public void SetTexture(string name, Texture texture)
        {
            SafetyCompute.RawCompute.SetTexture(_kernelIndex, name, texture);
        }
        public void SetTexture(int nameId, Texture texture)
        {
            SafetyCompute.RawCompute.SetTexture(_kernelIndex, nameId, texture);
        }

        #endregion
    }
}
﻿using UnityEngine;

public class TestSafetyComputeShader : TestSafetyBuffers
{
    public ComputeShader CoreShader;
    private SafetyComputeShader _compute;
    private SafetyComputeShader.Kernel _computeKernel;

    private SafetyBuffer<Vector4> _processedBuffer;

    protected override void OnEnable()
    {
        BuildArgsBuffer();

        _compute = new SafetyComputeShader(CoreShader);
        _computeKernel = _compute.FindKernel("CSMain");

        BuildPosBuffer();

        _computeKernel.SetBuffer("inputPositions", PosBuffer);
        _computeKernel.SetBuffer("outputPositions", _processedBuffer);
    }

    private new void BuildPosBuffer()
    {
        var data = GetPositionList();

        int bufferSize = _computeKernel.GetCompatibleBufferSize(data.Count);
        PosBuffer = new SafetyBuffer<Vector4>("SafePositionBuffer", bufferSize);
        PosBuffer.SetData(data);
        _processedBuffer = new SafetyBuffer<Vector4>("SafePositionBufferProcessed", bufferSize);
    }

    public override void OnDisable()
    {
        base.OnDisable();
        _processedBuffer.Dispose();
    }

    protected override void DrawInstanced(DrawObject drawObject, int index)
    {
        _compute.SetFloat("time", Time.time);

        _computeKernel.DispatchOver(PosBuffer);

        DrawInstanced(drawObject, index, _processedBuffer);
    }
}
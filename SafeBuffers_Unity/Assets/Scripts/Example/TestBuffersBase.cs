﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class TestBuffersBase : MonoBehaviour
{

    [Serializable]
    public class DrawObject
    {
        public Mesh Mesh;
        public int Submesh;
        public Material Material;
        public int Count;

        [NonSerialized]
        public int InstanceOffset;

    }

    public DrawObject[] Objects;
    protected MaterialPropertyBlock PropBlock;
    protected readonly Bounds GiantBounds = new Bounds(Vector3.zero, Vector3.one * 500);

    protected List<Vector4> GetPositionList()
    {
        var data = new List<Vector4>();
        for (int index = 0; index < Objects.Length; index++)
        {
            Objects[index].InstanceOffset = data.Count;
            int objCount = Objects[index].Count;
            for (int i = 0; i < objCount; i++)
            {
                data.Add(new Vector4((i - objCount / 2) * 2, index * 2, 0, 1)); //xyz = pos, w = scale
            }
        }

        return data;
    }

    private void Update()
    {
        if (PropBlock == null) PropBlock = new MaterialPropertyBlock();
        for (int index = 0; index < Objects.Length; index++)
        {
            var obj = Objects[index];
            PropBlock.SetInt("instanceOffset", obj.InstanceOffset);

            DrawInstanced(obj, index);
        }
    }

    protected abstract void DrawInstanced(DrawObject drawObject, int index);
}
﻿using System.Collections.Generic;
using UnityEngine;

public class TestSafetyBuffers : TestBuffersBase
{
    protected SafetyBuffer<DrawMeshInstancedIndirectArgs> ArgsBuffer;
    protected SafetyBuffer<Vector4> PosBuffer;

    protected virtual void OnEnable()
    {
        BuildArgsBuffer();
        BuildPosBuffer();
    }

    protected void BuildArgsBuffer()
    {
        var args = new DrawMeshInstancedIndirectArgs[Objects.Length];

        for (var index = 0; index < Objects.Length; index++)
        {
            var mesh = Objects[index].Mesh;
            var submesh = Objects[index].Submesh;
            args[index] = new DrawMeshInstancedIndirectArgs
            {
                IndexCount = mesh.GetIndexCount(submesh),
                StartIndex = mesh.GetIndexStart(submesh),
                StartVertex = mesh.GetBaseVertex(submesh),
                InstanceCount = (uint) Objects[index].Count
            };
        }

        ArgsBuffer = new SafetyBuffer<DrawMeshInstancedIndirectArgs>("SafeArgsBuffer", Objects.Length,
                ComputeBufferType.IndirectArguments);
        ArgsBuffer.SetData(args);
    }

    protected void BuildPosBuffer()
    {
        List<Vector4> data = GetPositionList();
        PosBuffer = new SafetyBuffer<Vector4>("SafePositionBuffer", data.Count);
        PosBuffer.SetData(data);
    }

    public virtual void OnDisable()
    {
        ArgsBuffer.Dispose();
        PosBuffer.Dispose();
    }

    protected override void DrawInstanced(DrawObject drawObject, int index)
    {
        DrawInstanced(drawObject, index, PosBuffer);
    }

    protected virtual void DrawInstanced(DrawObject drawObject, int index, SafetyBuffer<Vector4> drawPositionBuffer)
    {
        PropBlock.SetBuffer("positionBuffer", drawPositionBuffer);
        int argsOffsetInStructs = index;
        SafetyBufferExtensions.DrawMeshInstancedIndirect(
            drawObject.Mesh, drawObject.Submesh, drawObject.Material, GiantBounds,
            ArgsBuffer, argsOffsetInStructs, PropBlock);
    }
}
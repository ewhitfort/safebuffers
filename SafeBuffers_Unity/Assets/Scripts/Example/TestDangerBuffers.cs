﻿using System.Collections.Generic;
using UnityEngine;

public class TestDangerBuffers : TestBuffersBase
{
    protected ComputeBuffer ArgsBuffer;
    protected ComputeBuffer PosBuffer;

    protected virtual void OnEnable()
    {
        BuildArgsBuffer();
        BuildPosBuffer();
    }

    protected void BuildArgsBuffer()
    {
        var args = new uint[Objects.Length * 5];

        for (var index = 0; index < Objects.Length; index++)
        {
            var mesh = Objects[index].Mesh;
            var submesh = Objects[index].Submesh;
            args[index * 5 + 0] = mesh.GetIndexCount(submesh);
            args[index * 5 + 1] = (uint) Objects[index].Count;
            args[index * 5 + 2] = mesh.GetIndexStart(submesh);
            args[index * 5 + 3] = mesh.GetBaseVertex(submesh);
        }

        //args buffer must have stride=uint on some platforms, but not all
        ArgsBuffer = new ComputeBuffer(Objects.Length * 5, sizeof(uint), ComputeBufferType.IndirectArguments)
            {name = "DangerArgsBuffer"};
        ArgsBuffer.SetData(args);
    }

    protected void BuildPosBuffer()
    {
        List<Vector4> data = GetPositionList();
        PosBuffer = new ComputeBuffer(data.Count, sizeof(float) * 4) { name = "DangerPositionBuffer" };
        PosBuffer.SetData(data);
    }

    public virtual void OnDisable()
    {
        ArgsBuffer?.Dispose();
        PosBuffer?.Dispose();
    }

    protected override void DrawInstanced(DrawObject drawObject, int index)
    {
        DrawInstanced(drawObject, index, PosBuffer);
    }

    protected virtual void DrawInstanced(DrawObject drawObject, int index, ComputeBuffer computeBuffer)
    {
        PropBlock.SetBuffer("positionBuffer", computeBuffer);
        int argsOffsetInBytes = index * sizeof(uint) * 5;
        Graphics.DrawMeshInstancedIndirect(
            drawObject.Mesh, drawObject.Submesh, drawObject.Material, GiantBounds,
            ArgsBuffer, argsOffsetInBytes, PropBlock);
    }
}
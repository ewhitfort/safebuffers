﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TestGpuCulling : MonoBehaviour
{
    [Serializable]
    public class DrawObject
    {
        public Mesh Mesh;
        public int Submesh;
        public Material Material;
        public int Count;
    }

    struct CullInfo
    {
        public int StartIndex;
    };

    public DrawObject[] Objects;
    public ComputeShader CoreShader;

    public float CullXMin = -10;
    public float CullXMax = 10;
    public bool AnimateCullPositions;

    private readonly Bounds GiantBounds = new Bounds(Vector3.zero, Vector3.one * 500);

    private SafetyComputeShader _compute;

    private SafetyComputeShader.Kernel _clearArgsKernel;
    private SafetyComputeShader.Kernel _cullingKernel;
    private SafetyComputeShader.Kernel _modifyPositionsKernel;

    private SafetyBuffer<DrawMeshInstancedIndirectArgs> _argsBuffer;
    private SafetyBuffer<CullInfo> _cullInfoBuffer;
    private SafetyBuffer<Vector4> _sourcePositionBuffer;
    private SafetyBuffer<Vector4> _modifiedBuffer;
    private SafetyBuffer<Vector4> _drawnPosBuffer;
    private SafetyBuffer<int> _indexBuffer;

    private MaterialPropertyBlock PropBlock;

    private CullInfo[] _cullInfo;

    protected void OnEnable()
    {
        _compute = new SafetyComputeShader(CoreShader);
        _clearArgsKernel = _compute.FindKernel("CSClearArgs");
        _cullingKernel = _compute.FindKernel("CSCulling");
        _modifyPositionsKernel = _compute.FindKernel("CSModifyPositions");

        BuildArgsBuffer();
        BuildPosBuffer();

        _modifyPositionsKernel.SetBuffer("inputPositions", _sourcePositionBuffer);
        _modifyPositionsKernel.SetBuffer("outputPositions", _modifiedBuffer);

        _clearArgsKernel.SetBuffer("argsBuffer", _argsBuffer);

        _cullingKernel.SetBuffer("argsBuffer", _argsBuffer);
        _cullingKernel.SetBuffer("inputBatch", _indexBuffer);
        _cullingKernel.SetBuffer("cullInfoBuffer", _cullInfoBuffer);
        _cullingKernel.SetBuffer("inputPositions", _modifiedBuffer);
        _cullingKernel.SetBuffer("outputPositions", _drawnPosBuffer);

        PropBlock = new MaterialPropertyBlock();
        PropBlock.SetBuffer("positionBuffer", _drawnPosBuffer);
    }

    private void OnDrawGizmosSelected()
    {
        //Preview the culling region to confirm it matches what's drawn
        Gizmos.DrawWireCube(new Vector3((CullXMax+CullXMin)/2,0,0),new Vector3(CullXMax-CullXMin,10,0));
    }


    private void Update()
    {
        if (AnimateCullPositions) CullXMin = Mathf.Lerp(-CullXMax, CullXMax * 0.2f, Mathf.Cos(Time.time / 2) / 2 + 0.5f);

        _compute.SetFloat("time", Time.time);
        _compute.SetVector("cullWindow", new Vector4(CullXMin, CullXMax, 0, 0));

        _modifyPositionsKernel.DispatchOver(_sourcePositionBuffer);

        _clearArgsKernel.DispatchOver(_argsBuffer);
        _cullingKernel.DispatchOver(_sourcePositionBuffer);


        for (int index = 0; index < Objects.Length; index++)
        {
            PropBlock.SetInt("instanceOffset", _cullInfo[index].StartIndex);

            var drawObject = Objects[index];
            int argsOffsetInStructs = index;
            SafetyBufferExtensions.DrawMeshInstancedIndirect(
                drawObject.Mesh, drawObject.Submesh, drawObject.Material, GiantBounds,
                _argsBuffer, argsOffsetInStructs, PropBlock);
        }
    }

    public void OnDisable()
    {
        _argsBuffer.Dispose();
        _cullInfoBuffer.Dispose();
        _modifiedBuffer.Dispose();
        _sourcePositionBuffer.Dispose();
        _drawnPosBuffer.Dispose();
        _indexBuffer.Dispose();
    }

    /// <summary>
    /// Build some Args that can be used by DrawMeshInstancedIndirect.
    /// We'll override the InstanceCount in our culling code but it's left in here to show how it fits in.
    /// </summary>
    protected void BuildArgsBuffer()
    {
        int argsCount = _cullingKernel.GetCompatibleBufferSize(Objects.Length);

        // DrawMeshInstancedIndirectArgs is a wrapper struct that simplifies this usage
        var args = new DrawMeshInstancedIndirectArgs[argsCount];

        for (var index = 0; index < Objects.Length; index++)
        {
            var mesh = Objects[index].Mesh;
            int submesh = Objects[index].Submesh;
            args[index] = new DrawMeshInstancedIndirectArgs
            {
                IndexCount = mesh.GetIndexCount(submesh),
                StartIndex = mesh.GetIndexStart(submesh),
                StartVertex = mesh.GetBaseVertex(submesh),
                InstanceCount = (uint)Objects[index].Count
            };
        }

        //This will actually create a Buffer with an internal stride of sizeof(uint) and length of 5*argsCount for compatibility with more platforms
        // (don't worry about that if this is new to you, but if you roll your own solution some consoles will get funny if you don't do that)
        _argsBuffer = new SafetyBuffer<DrawMeshInstancedIndirectArgs>("SafeArgsBuffer", argsCount, ComputeBufferType.IndirectArguments);
        _argsBuffer.SetData(args);
    }

    private void BuildPosBuffer()
    {
        GetPositionAndIndexLists(out var data, out var indexes, out _cullInfo);

        int bufferSize = _modifyPositionsKernel.GetCompatibleBufferSize(data.Count);

        _sourcePositionBuffer = new SafetyBuffer<Vector4>("SafePositionBuffer", bufferSize);
        _indexBuffer = new SafetyBuffer<int>("CullIndexBuffer", bufferSize);

        _sourcePositionBuffer.SetData(data);
        _indexBuffer.SetData(indexes);

        //output: doesn't have a CPU equivalent to set from
        _modifiedBuffer = new SafetyBuffer<Vector4>("SafePositionBufferProcessed", bufferSize);
        _drawnPosBuffer = new SafetyBuffer<Vector4>("SafePositionBufferCulledForDraw", bufferSize);
        
        _cullInfoBuffer = new SafetyBuffer<CullInfo>("CullInfoBuffer", _cullInfo.Length);
        _cullInfoBuffer.SetData(_cullInfo);
    }

    private void GetPositionAndIndexLists(out List<Vector4> positions, out List<int> indexes, out CullInfo[] cullInfo)
    {
        positions = new List<Vector4>();
        indexes = new List<int>();

        cullInfo = new CullInfo[Objects.Length];

        //We pad our buffers out so that each cullingKernel wavefront has a bunch of the same index, then -1 for the rest of the group
        

        for (int index = 0; index < Objects.Length; index++)
        {
            cullInfo[index] = new CullInfo(){ StartIndex = positions.Count};

            int realObjectCount = Objects[index].Count;
            int paddedSize = _cullingKernel.GetCompatibleBufferSize(realObjectCount);
            int padding = paddedSize - realObjectCount;

            for (int i = 0; i < realObjectCount; i++)
            {
                positions.Add(new Vector4((i - realObjectCount / 2) * 2, index * 2, 0, 1)); //xyz = pos, w = scale
                indexes.Add(index);
            }
            //We'll pad with index of -1 (to tell the culling not to use it) and arbitrary positions
            for (int i = 0; i < padding; i++)
            {
                positions.Add(default);
                indexes.Add(-1);
            }
        }
    }

}
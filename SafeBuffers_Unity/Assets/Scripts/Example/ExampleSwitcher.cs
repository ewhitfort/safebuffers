﻿using UnityEngine;

public class ExampleSwitcher : MonoBehaviour
{
    public GameObject[] ExampleObjects;

    private void OnGUI()
    {
        foreach (var exampleObject in ExampleObjects)
        {
            GUI.enabled = !exampleObject.activeSelf;
            if (GUILayout.Button(exampleObject.name))
                SetExample(exampleObject);
        }
    }

    private void SetExample(GameObject activate)
    {
        foreach (var exampleObject in ExampleObjects)
        {
            exampleObject.SetActive(exampleObject == activate);
        }
    }
}
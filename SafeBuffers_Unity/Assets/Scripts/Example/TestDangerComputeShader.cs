﻿using UnityEngine;

public class TestDangerComputeShader : TestDangerBuffers
{
    public ComputeShader Compute;
    private int _computeKernel;
    private uint _threadGroupSizeX;

    private ComputeBuffer _processedBuffer;

    protected override void OnEnable()
    {
        BuildArgsBuffer();

        _computeKernel = Compute.FindKernel("CSMain");
        Compute.GetKernelThreadGroupSizes(_computeKernel, out _threadGroupSizeX, out _, out _);

        BuildPosBuffer();

        Compute.SetBuffer(_computeKernel, "inputPositions", PosBuffer);
        Compute.SetBuffer(_computeKernel, "outputPositions", _processedBuffer);
    }

    private new void BuildPosBuffer()
    {
        var data = GetPositionList();

        int bufferSize = (int) (_threadGroupSizeX * ((data.Count - 1) / _threadGroupSizeX + 1));
        PosBuffer = new ComputeBuffer(bufferSize, sizeof(float) * 4) {name = "DangerPositionBuffer"};
        PosBuffer.SetData(data);
        _processedBuffer = new ComputeBuffer(bufferSize, sizeof(float) * 4) {name = "DangerPositionBufferProcessed"};
    }

    public override void OnDisable()
    {
        base.OnDisable();
        _processedBuffer?.Dispose();
    }

    protected override void DrawInstanced(DrawObject drawObject, int index)
    {
        Compute.SetFloat("time", Time.time);
        
        Compute.Dispatch(_computeKernel, PosBuffer.count / (int)_threadGroupSizeX, 1,1);

        DrawInstanced(drawObject, index, _processedBuffer);
    }
}
﻿// Each #kernel tells which function to compile; you can have many kernels
#pragma kernel CSModifyPositions
#pragma kernel CSCulling
#pragma kernel CSClearArgs

struct Args {
	uint IndexCount;
	uint InstanceCount;
	uint StartIndex;
	uint StartVertex;
	uint BrokenUnused_StartIndex; // doesn't work on many platforms: see SafetyBufferUtility.cs
};

struct CullInfo {
	int startIndex;
};

StructuredBuffer<float4> inputPositions;
RWStructuredBuffer<float4> outputPositions;

StructuredBuffer<int> inputBatch;
RWStructuredBuffer<Args> argsBuffer;
StructuredBuffer<CullInfo> cullInfoBuffer;

float time;

float2 cullWindow;

[numthreads(8,1,1)]
void CSModifyPositions(uint3 id : SV_DispatchThreadID)
{
	float4 pos = inputPositions[id.x];
	pos.w = 1 + 0.2 * sin(pos.x + time * 2.2);
	pos.y += 0.2 * sin(pos.x + pos.y + time);
	outputPositions[id.x] = pos;
}

[numthreads(8, 1, 1)]
void CSClearArgs(uint3 id : SV_DispatchThreadID)
{
	argsBuffer[id.x].InstanceCount = 0;
}

groupshared uint visibleCount = 0;
groupshared uint bufferPos;
groupshared uint batchIndex;
groupshared uint batchIndexDrawAs;

[numthreads(8,1,1)]
void CSCulling (uint3 dispatchThreadID : SV_DispatchThreadID, uint localIndex : SV_GroupIndex)
{

	if (localIndex == 0)
	{
		batchIndex = inputBatch[dispatchThreadID.x];
		visibleCount = 0;
		bufferPos = 0;
	}

	GroupMemoryBarrierWithGroupSync();

	bool shouldDraw = false;
	if (batchIndex != -1)
	{
		float4 pos = inputPositions[dispatchThreadID.x];

		// Do frustum/hi-Z/whatever culling here. To keep this simple,
		// we check if the position plus/minus half the scale is within the values 
		// passed into cullWindow. 
		// (all example primitives have a width of 1, normally you'd put a size in CullInfo)
		float halfWidthScaled = pos.w / 2;
		shouldDraw =
			inputBatch[dispatchThreadID.x] != -1 &&
			(pos.x + halfWidthScaled > cullWindow.x && pos.x - halfWidthScaled < cullWindow.y);

		uint myCount = 0;
		if (shouldDraw)
		{
			InterlockedAdd(visibleCount, 1, myCount);
		}

		GroupMemoryBarrierWithGroupSync();

		uint index = 0;
		if (localIndex == 0)
		{
			InterlockedAdd(argsBuffer[batchIndex].InstanceCount, visibleCount, bufferPos);
		}

		GroupMemoryBarrierWithGroupSync();

		if (shouldDraw)
		{
			index = abs(((int)(bufferPos + myCount)) + cullInfoBuffer[batchIndex].startIndex);
			outputPositions[index] = inputPositions[dispatchThreadID.x];
		}

		GroupMemoryBarrierWithGroupSync();
	}

}
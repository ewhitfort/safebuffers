﻿using System;

/// <summary>
/// A useful struct for using with SafetyBuffers and DrawMeshInstancedIndirect calls
/// </summary>
public struct DrawMeshInstancedIndirectArgs
{
    public uint IndexCount;
    public uint InstanceCount;
    public uint StartIndex;
    public uint StartVertex;

    /// <summary>
    /// Start instance doesn't work on most platforms (possibly DirectX platforms). A card reporting this was marked as "By Design" by unity
    /// https://issuetracker.unity3d.com/issues/drawmeshinstancedindirect-start-instance-location-doesnt-work-on-windows
    ///
    /// Unity's response as to why it's marked as "By Design" (in email)
    ///     From the docs page for D3D11:
    ///     https://msdn.microsoft.com/en-us/library/windows/desktop/ff476410(v=vs.85).aspx
    ///     "A value added to each index before reading per-instance data from a vertex buffer."
    ///     Note that this means the offset is applied to the Index, not to SV_InstancedID.
    ///     Based on the speculation in the description about iOS/PS4/XOne, it is possible (though not verified)
    ///     that other platforms apply this value differently, which is causing the different behavior on those platforms.
    ///     However, we have no access to how that value is applied on Windows - it is performed by the D3D11 driver.
    ///     I can only suggest that the user passes this offset as a shader constant instead.
    ///
    /// Passing by shader constant on the material or propertyBlock works and is what I'd recommend too.
    /// (If anyone has a fix for this I'd love to see it. I haven't revisited this problem since ~2017 or 18)
    /// </summary>
    [Obsolete("This doesn't work on most platforms. Listed as 'By Design' by Unity", true)]
    public uint StartInstance;
}
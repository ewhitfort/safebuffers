﻿using System;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.Rendering;

public readonly struct SafetyBuffer<T> : IDisposable, SafetyBufferChecker.ISafetyBufferCheck where T: struct
{
    //Use the first value for most padding, and it reversed for the first and last value of padding
    private const uint UninitialisedValue         = 0x_E111E_CA7; //decimal 3776048295
    private const uint UninitialisedValueReversed = 0x_CA7_E111E; //decimal 2060325150

    private const int GoalBytesOfPadding = 128; //This is a minimum; it will be slightly longer for strides that don't divide into this
    
    public static int Stride { get; } = UnsafeUtility.SizeOf<T>();

    public readonly string Name;
    public readonly int Count;

    /// <summary>
    /// This shouldn't be used by anything other than extension methods.
    /// </summary>
    internal ComputeBuffer InternalComputeBuffer { get; }

    public SafetyBuffer(string name, int count, ComputeBufferType type = ComputeBufferType.Default, SafetyBufferChecker.DebugMode debug = SafetyBufferChecker.DebugMode.None)
    {
        Name = name;
        Count = count;
        debug |= SafetyBufferChecker.GlobalDebugMode;

        var storeWithStride = Stride;
        // Some platforms require IndirectArguments to be stored with a stride of sizeof(uint) (i.e. 4).
        // Enforce that here so that we can have nice structures elsewhere
        bool storeAsIntBuffer = (type & ComputeBufferType.IndirectArguments) != 0;
        if (storeAsIntBuffer)
        {
            count *= storeWithStride / sizeof(uint);
            storeWithStride = sizeof(uint);
        }

        int padCount = 0;
        if (debug != SafetyBufferChecker.DebugMode.None)
        {
            padCount = (GoalBytesOfPadding - 1)/storeWithStride + 1;
        }

        InternalComputeBuffer = new ComputeBuffer(count + padCount, storeWithStride, type) { name = name };

        if (debug != SafetyBufferChecker.DebugMode.None)
        {
            int start = 0;
            if ((debug & SafetyBufferChecker.DebugMode.ForceUninitializedBuffer) == 0) // all other modes only need the end initialized
            {
                start = count * storeWithStride / sizeof(uint);
            }
            SetDataUninitialized(start);

            if ((debug & SafetyBufferChecker.DebugMode.CheckPadding) != 0)
            {
                SafetyBufferChecker.AddToPaddingCheck(this);
            }
        }
    }

    private void SetDataUninitialized(int start = 0)
    {
        var data = new NativeArray<uint>((InternalComputeBuffer.count * InternalComputeBuffer.stride) / sizeof(uint) - start,
            Allocator.Temp, NativeArrayOptions.UninitializedMemory);
        for (int i = 0; i < data.Length; i++)
        {
            data[i] = UninitialisedValue;
        }

        data[0] = UninitialisedValueReversed;
        data[data.Length-1] = UninitialisedValueReversed;

        InternalComputeBuffer.SetData(data, 0, start, data.Length);

        data.Dispose();
    }

    public void Dispose()
    {
        InternalComputeBuffer?.Dispose();
        SafetyBufferChecker.RemoveFromPaddingCheck(this);
    }

    public void SafetyCheck()
    {
        int paddingStart = Count*Stride;
        int size = (InternalComputeBuffer.count * InternalComputeBuffer.stride) - paddingStart;
        AsyncGPUReadback.Request(InternalComputeBuffer, size, paddingStart, ValidateResults);
    }

    private void ValidateResults(AsyncGPUReadbackRequest readBackRequest)
    {
        var nativeArray = readBackRequest.GetData<uint>();

        int corruptedChunks = 0;

        bool leadCorrupt = false, endCorrupt = false;

        if (nativeArray[0] != UninitialisedValueReversed)
        {
            corruptedChunks++;
            leadCorrupt = true;
        }

        if (nativeArray[nativeArray.Length - 1] != UninitialisedValueReversed)
        {
            corruptedChunks++;
            endCorrupt = true;
        }

        for (var index = 1; index < nativeArray.Length - 1; index++)
        {
            if (nativeArray[index] != UninitialisedValue) corruptedChunks++;
        }

        if (corruptedChunks > 0)
        {
            Debug.LogErrorFormat("Corrupted memory in buffer {0}. {1} of {2} checkValues(uint) overridden (first={3},last={4})", Name, corruptedChunks, nativeArray.Length, leadCorrupt, endCorrupt);
        }
    }

#region SetData

    /// <summary>
    ///   <para>Set the buffer with values from an array.</para>
    /// </summary>
    /// <param name="data">Array of values to fill the buffer.</param>
    public void SetData(T[] data)
    {
        InternalComputeBuffer.SetData(data);
    }

    /// <summary>
    ///   <para>Set the buffer with values from a List.</para>
    /// </summary>
    /// <param name="data">List of values to fill the buffer.</param>
    public void SetData(List<T> data)
    {
        InternalComputeBuffer.SetData(data);
    }

    /// <summary>
    ///   <para>Set the buffer with values from a NativeArray.</para>
    /// </summary>
    /// <param name="data">NativeArray of values to fill the buffer.</param>
    public void SetData(NativeArray<T> data)
    {
        InternalComputeBuffer.SetData(data);
    }

    /// <summary>
    ///   <para>Partial copy of data values from an array into the buffer.</para>
    /// </summary>
    /// <param name="data">Array of values to fill the buffer.</param>
    /// <param name="managedBufferStartIndex">The first element index in data to copy to the compute buffer.</param>
    /// <param name="computeBufferStartIndex">The first element index in compute buffer to receive the data.</param>
    /// <param name="count">The number of elements to copy.</param>
    public void SetData(T[] data, int managedBufferStartIndex, int computeBufferStartIndex, int count)
    {
        InternalComputeBuffer.SetData(data, managedBufferStartIndex, computeBufferStartIndex, count);
    }

#endregion
}

/// <summary>
/// Extension methods to make SafetyBuffers interact with builtin things like CommandBuffer, MaterialPropertyBlocks and similar
/// like regular ComputeBuffers. 
/// </summary>
public static class SafetyBufferExtensions
{
    public static void SetComputeFloatParam(this CommandBuffer buffer, SafetyComputeShader computeShader, string name, float val)
    {
        buffer.SetComputeFloatParam(computeShader.RawCompute,name, val);
    }
    public static void SetComputeIntParam(this CommandBuffer buffer, SafetyComputeShader computeShader, string name, int val)
    {
        buffer.SetComputeIntParam(computeShader.RawCompute, name, val);
    }

    public static void DispatchComputeOver<T>(
        this CommandBuffer buffer,
        SafetyComputeShader.Kernel kernel,
        SafetyBuffer<T> safetyBuffer) where T : struct
    {
        kernel.DispatchOver(safetyBuffer, buffer);
    }

    public static void SetBuffer<T>(this ComputeShader shader, int kernelIndex, string nameId, SafetyBuffer<T> buffer) where T : struct
    {
        shader.SetBuffer(kernelIndex, nameId, buffer.InternalComputeBuffer);
    }
    public static void SetBuffer<T>(this Material material,string nameId, SafetyBuffer<T> buffer) where T : struct
    {
        material.SetBuffer(nameId, buffer.InternalComputeBuffer);
    }
    public static void SetBuffer<T>(this MaterialPropertyBlock block, string nameId, SafetyBuffer<T> buffer) where T : struct
    {
        block.SetBuffer(nameId, buffer.InternalComputeBuffer);
    }

    public static void DrawMeshInstancedIndirect(Mesh mesh, int subMeshIndex, Material material, Bounds bounds,
        SafetyBuffer<DrawMeshInstancedIndirectArgs> buffer, int argsOffsetInStructs, MaterialPropertyBlock properties = null)
    {
        Graphics.DrawMeshInstancedIndirect(mesh, subMeshIndex, material, bounds, buffer.InternalComputeBuffer, argsOffsetInStructs * SafetyBuffer<DrawMeshInstancedIndirectArgs>.Stride, properties);
    }
}